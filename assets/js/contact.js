// Responsive view
responsiveOffice();
window.addEventListener("resize", responsiveOffice);

function responsiveOffice() {
  const officeDesktop = document.querySelector("#officeDesktop");
  const officeMobile = document.querySelector("#officeMobile");

  if (screen.width <= 768) {
    //   officeDesktop.classList.toggle("show");
    officeDesktop.style.display = "none";
    officeMobile.style.display = "block";
  } else {
    officeDesktop.style.display = "flex";
    officeMobile.style.display = "none";
  }
}

// Swiper
const swiperOffice = new Swiper(".swiper-office__container", {
    direction: "horizontal",
    loop: true,
    slidesPerView: 1,
    spaceBetween: 30,
    centeredSlides: true,
    navigation: {
      nextEl: "#swiper-button-next__office",
      prevEl: "#swiper-button-prev__office",
    },
    breakpoints: {
      475: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
    },
  });