// Transparent Navbar on Top
$(window).scroll(function () {
  if ($(document).scrollTop() > 50) {
    $(".navbar").addClass("navbar__scrolled");
  } else {
    $(".navbar").removeClass("navbar__scrolled");
  }
});


let hero = document.querySelector(".section-hero");
hero.style.height = $(window).height() + "px";

// Swiper Hero Section
const swiperHero = new Swiper(".swiper-hero", {
  effect: "fade",
  direction: "horizontal",
  loop: true,
  slidesPerView: 1,
  spaceBetween: 30,
  // allowTouchMove:false,
  pagination: {
    el: ".swiper-pagination",
    type: "fraction",
  },
  navigation: {
    nextEl: "#swiper-button-next__hero",
    prevEl: "#swiper-button-prev__hero",
  },
});

// Swiper Benefit Section
const swiperBenefit = new Swiper(".swiper-benefit", {
  loop: true,
  slidesPerView: 1,
  spaceBetween: 30,
  breakpoints: {
    992: {
      slidesPerView: 3,
      spaceBetween: 20,
    },
  },
  navigation: {
    nextEl: "#swiper-button-next__benefit",
    prevEl: "#swiper-button-prev__benefit",
  },
});
