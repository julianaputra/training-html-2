// Toggle Collapse
const button = document.querySelector("#overviewButton");
const container = document.querySelector("#overviewContainer");
const content = document.querySelector("#overviewContent");

button.addEventListener("click", function () {
  container.classList.toggle("show");
  container.style.height = content.offsetHeight + "px";
  if(container.classList.contains("show")) {
    button.innerHTML = `Finish Reading <i class="fas fa-angle-up"></i>`
  } else {
    button.innerHTML = `Continue Reading <i class="fas fa-angle-down"></i>`
  }
});

// Separator
toggleSeparator();
window.addEventListener('resize', toggleSeparator);

function toggleSeparator() {
  const amenities = document.querySelectorAll(".separatorAmenities");
  const details = document.querySelectorAll(".separatorDetails");

  if (screen.width < 576) {
    for (let i = 0; i < amenities.length; i++) {
      if(i < 1) {
        amenities[i].style.display = "none";
      }
      else {
        amenities[i].style.display = "block";
      }
    }
    for (let i = 0; i < details.length; i++) {
      if(i < 1) {
        details[i].style.display = "none";
      }
      else {
        details[i].style.display = "block";
      }
    }
  } else {
    for (let i = 0; i < amenities.length; i++) {
      if(i < 3) {
        amenities[i].style.display = "none";
      }
      else {
        amenities[i].style.display = "block";
      }
    }
    for (let i = 0; i < details.length; i++) {
      if(i < 2) {
        details[i].style.display = "none";
      }
      else {
        details[i].style.display = "block";
      }
    }
  }
}

// Lightcase
jQuery(document).ready(function ($) {
  $("a[data-rel^=lightcase]").lightcase();
});

// Chart
const data = {
  labels: ["Principal and Interest", "Property Taxes", "HOA Dues"],
  datasets: [
    {
      data: [23565, 1068, 2036],
      backgroundColor: ["#0070c9", "#4b9ad9", "#99c6e9"],
      borderColor: ["#fff", "#fff", "#fff"],
      borderWidth: 3,
    },
  ],
};

var config = {
  type: "doughnut",
  data,
  options: {
    cutout: "85%",
    plugins: {
      legend: {
        display: false,
      },
    },
  },
};

var ctx = document.getElementById("paymentChart").getContext("2d");

var paymentChart = new Chart(ctx, config);
