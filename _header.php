<header class="header">
    <nav class="navbar fixed-top">
        <div class="container">
            <div class="navbar__hamburger">
                <span class="navbar__bar"></span>
                <span class="navbar__bar"></span>
                <span class="navbar__bar"></span>
            </div>
            <a href="index.php" class="navbar__logo">resideo.</a>
            <ul class="navbar__menu">
                <li class="navbar__item">
                    <a href="index.php" class="navbar__link">Home</a>
                </li>
                <li class="navbar__item">
                    <a href="property-listing.php" class="navbar__link">Properties</a>
                </li>
                <li class="navbar__item">
                    <a href="#" class="navbar__link">Agents</a>
                </li>
                <li class="navbar__item">
                    <a href="#" class="navbar__link">Blog</a>
                </li>
                <li class="navbar__item">
                    <a href="#" class="navbar__link">Themes</a>
                </li>
                <li class="navbar__item">
                    <a href="contact.php" class="navbar__link">Contact Us</a>
                </li>
            </ul>
            <a href="#" class="navbar__profile" data-toggle="modal" data-target="#loginModal"><i class="far fa-user"></i></a>
        </div>
    </nav>
</header>

<!-- Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content modal-login">
            <div class="modal-header border-0">
                <h5 class="modal-title font-weight-bold">Welcome back!</h5>
                <button type="button" class="close modal-login__close-icon" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label modal-login__label">Email</label>
                        <input type="text" class="form-control" placeholder="Enter your email address">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label modal-login__label">Password</label>
                        <input type="password" class="form-control" placeholder="Enter your password">
                    </div>
                    <button type="button" class="btn btn-primary w-100 mb-4">Sign In</button>
                    <div class="text-center">
                        <a href="javascript:void(0)"><u>Forgot Password</u></a>
                        <p class="mt-3">New to Resideo? <a href="javascript:void(0)"><u>Create an account</u></a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>