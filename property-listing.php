<?php
$properties = [
    [
        "title" => "Chich Apartment in Downtown",
        "price" => "$890,000",
        "facility" => "2 BD | 2 BA | 920 SF",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "image" => "assets/img/prop-4-1-gallery.jpg"
    ],
    [
        "title" => "Colorful Little Apartment",
        "price" => "$2,674",
        "facility" => "1 BD | 1 BA | 500 SF",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "image" => "assets/img/prop-5-1-gallery.jpg"
    ],
    [
        "title" => "Cozy Two Bedroom Apartment",
        "price" => "$960,000",
        "facility" => "2 BD | 2 BA | 870 SF",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "image" => "assets/img/prop-6-1-gallery.jpg"
    ],
    [
        "title" => "Beautiful House in Marina",
        "price" => "$5,198,000",
        "facility" => "5 BD | 4.5 BA | 3,945 SF",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "image" => "assets/img/prop-10-1-gallery.jpg"
    ],
    [
        "title" => "Modern Residence",
        "price" => "$7,995",
        "facility" => "4 BD | 1.5 BA | 2,240 SF",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "image" => "assets/img/prop-11-1-gallery.jpg"
    ],
    [
        "title" => "Luxury Manison",
        "price" => "$5,430,000",
        "facility" => "4 BD | 5 BA | 5,200 SF",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "image" => "assets/img/prop-12-1-gallery.jpg"
    ],
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resideo | Property Listing</title>
    <?php include '_global-style.php' ?>
    <link rel="stylesheet" href="assets/css/pages/property-listing.css">
    <link rel="stylesheet" href="assets/css/components/card-property.css">
</head>

<body>
    <?php include '_header.php' ?>
    <main class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="filter">
                        <form action="#">
                            <div class="form-row mb-3">
                                <div class="col-12 col-sm order-sm-2">
                                    <div class="input-group mb-3 mb-sm-0">
                                        <input class="form-control form-input" type="text" placeholder="Search by City, Neighborhood, or Address">
                                        <div class="input-group-append">
                                            <button class="btn button-form" type="button"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-sm-3 order-sm-1">
                                    <select class="form-control form-input">
                                        <option>Buy</option>
                                        <option>Rent</option>
                                    </select>
                                </div>
                                <div class="col-auto order-sm-3">
                                    <button type="button" class="btn button-form" data-toggle="modal" data-target="#filterModal">
                                        <i class="fas fa-filter"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="form-row align-items-center">
                                <div class="col col-sm-auto order-sm-2">
                                    <select class="form-control form-input">
                                        <option>Highest Price</option>
                                        <option>Lower Price</option>
                                    </select>
                                </div>
                                <div class="col-auto order-sm-3">
                                    <button type="button" class="btn button-form">
                                        <i class="far fa-map"></i>
                                    </button>
                                </div>
                                <div class="col-12 col-sm">
                                    <p class="filter__result  mt-3 mt-sm-0">1,684 Results</p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <?php foreach ($properties as $property) { ?>
                            <div class="col-12 col-lg-6">
                                <a href="property-detail.php" class="card card-property">
                                    <div class="card-property__image-container">
                                        <img class="card-property__image" src="<?= $property['image'] ?>" alt="<?= $property['title'] . ' Image' ?>">
                                    </div>
                                    <div class="card-body card-property__body">
                                        <h3 class="card-property__title"><?= $property['title'] ?></h3>
                                        <p class="card-property__price"><?= $property['price'] ?></p>
                                        <p class="card-property__facility"><?= $property['facility'] ?></p>
                                        <p class="card-property__desc"><?= $property['desc'] ?></p>
                                    </div>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                    <nav class="mb-4">
                        <ul class="pagination">
                            <li class="pagination__item"><a class="pagination__link active" href="javascript:void(0)">1</a></li>
                            <li class="pagination__item"><a class="pagination__link" href="javascript:void(0)">2</a></li>
                            <li class="pagination__item"><a class="pagination__link" href="javascript:void(0)">3</a></li>
                            <li class="pagination__item"><a class="pagination__link" href="javascript:void(0)">Next</a><i class="fas fa-angle-right"></i></li>
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
        <div class="map-container">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.104066007387!2d115.23826581460467!3d-8.681653193762244!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd240f88bb37a69%3A0xdf8443f1fabfe305!2sPT.Timedoor%20Indonesia!5e0!3m2!1sen!2sid!4v1621926072708!5m2!1sen!2sid" class="map" allowfullscreen="" loading="lazy"></iframe>
        </div>
    </main>

    <!-- Filter Modal -->
    <div class="modal fade" id="filterModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Filter Location</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="0">
                        <label class="form-check-label" for="0">
                            New York
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="1">
                        <label class="form-check-label" for="1">
                            Los Angeles
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="2">
                        <label class="form-check-label" for="2">
                            San Francisco
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="2">
                        <label class="form-check-label" for="2">
                            Las Vegas
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer-property">
        <div class="container-fluid">
            <p class="m-0">&copy Resideo. All Rights Reserved. 2019</p>
        </div>
    </footer>
    <?php include '_global-script.php' ?>
</body>

</html>