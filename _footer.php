      <!-- Footer -->
      <footer class="section footer">
          <div class="container">
              <div class="row">
                  <div class="col-12 col-md-3 col-lg-3 mb-5">
                      <h3 class="footer__logo">resideo.</h3>
                      <ul class="footer__list footer__address">
                          <li class="footer__item">90 Fifth Avenue, 3rd Floor</li>
                          <li class="footer__item">San Francisco, CA 1980</li>
                          <li class="footer__item">(123) 456-7890</li>
                      </ul>
                      <div class="footer__social">
                          <a href="#" class="footer__social-link"><i class="fab fa-instagram"></i></a>
                          <a href="#" class="footer__social-link"><i class="fab fa-facebook-square"></i></a>
                          <a href="#" class="footer__social-link"><i class="fab fa-twitter"></i></a>
                      </div>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3 mb-5">
                      <h4 class="footer__title">Company</h4>
                      <ul class="footer__list">
                          <li class="footer__item">
                              <a href="#" class="footer__link">About Us</a>
                          </li>
                          <li class="footer__item">
                              <a href="#" class="footer__link">Agents</a>
                          </li>
                          <li class="footer__item">
                              <a href="#" class="footer__link">Blog</a>
                          </li>
                          <li class="footer__item">
                              <a href="#" class="footer__link">Demos</a>
                          </li>
                          <li class="footer__item">
                              <a href="#" class="footer__link">Contact Us</a>
                          </li>
                      </ul>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3 mb-5">
                      <h4 class="footer__title">Actions</h4>
                      <ul class="footer__list">
                          <li class="footer__item">
                              <a href="#" class="footer__link">Buy Properties</a>
                          </li>
                          <li class="footer__item">
                              <a href="#" class="footer__link">Rent Properties</a>
                          </li>
                          <li class="footer__item">
                              <a href="#" class="footer__link">Sell Properties</a>
                          </li>
                      </ul>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3 mb-5">
                      <h4 class="footer__title">Explore</h4>
                      <ul class="footer__list">
                          <li class="footer__item">
                              <a href="#" class="footer__link">Homes for Rent</a>
                          </li>
                          <li class="footer__item">
                              <a href="#" class="footer__link">Apartments for Rent</a>
                          </li>
                          <li class="footer__item">
                              <a href="#" class="footer__link">Homes for Sale</a>
                          </li>
                          <li class="footer__item">
                              <a href="#" class="footer__link">Apartments for Sale</a>
                          </li>
                          <li class="footer__item">
                              <a href="#" class="footer__link">CRM</a>
                          </li>
                      </ul>
                  </div>
              </div>
              <div class="footer__bottom-text">
                  <a class="footer__term" href="#">Term & Conditions and Privacy Policy</a>
                  <p class="footer__copyright">&copy Resideo. All Rights Reserved. 2019</p>
              </div>
          </div>
      </footer>