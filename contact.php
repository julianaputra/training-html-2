<?php
$locations = [
    [
        'id' => 1,
        'location' => 'Los Angeles',
        'address' => '90 Fifth Avenue, 3rd Floor',
        'region' => 'Los Angeles, CA 1980',
        'phone' => '(123) 789-7390',
        'email' => 'office-la@resideo.com',
        "map" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.591250815591!2d115.20391151460444!3d-8.635180093794657!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd23f46af664051%3A0xf4022ebd24848749!2sTerminal%20Bus%20Ubung!5e0!3m2!1sen!2sid!4v1623003450088!5m2!1sen!2sid"
    ],
    [
        'id' => 2,
        'location' => 'New York',
        'address' => '90 Fifth Avenue, 3rd Floor',
        'region' => 'New York, NY 1981',
        'phone' => '(456) 789-7390',
        'email' => 'office-ny@resideo.com',
        "map" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.2710633218185!2d115.23146931460471!3d-8.665750993773372!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd2408a4df3884b%3A0x792d1d79cc11a3d2!2sMIE%20AYAM%20JAKARTA!5e0!3m2!1sen!2sid!4v1623003361317!5m2!1sen!2sid"
    ],
    [
        'id' => 3,
        'location' => 'San Francisco',
        'address' => '90 Fifth Avenue, 3rd Floor',
        'region' => 'San Francisco, CA 1982',
        'phone' => '(321) 789-7390',
        'email' => 'office-sf@resideo.com',
        "map" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.3677136260794!2d115.21023061460464!3d-8.656534293779776!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd240a20b08cb11%3A0xc0600ec21e227e5e!2sBadung%20Market!5e0!3m2!1sen!2sid!4v1623003406613!5m2!1sen!2sid"
    ],
]
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resideo | Contact</title>
    <?php include '_global-style.php' ?>
    <link rel="stylesheet" href="assets/css/pages/contact.css">
</head>

<body>
    <?php include '_header.php' ?>
    <main class="main">
        <section class="container pt-5 pb-3">
            <h1 class="title">Contact Us</h1>
            <p>Say hello. Tell us how we can guide you.</p>
        </section>
        <section class="section-office">
            <div class="container">
                <div id="officeMobile" class="swiper-office">
                    <div class="swiper-container swiper-office__container">
                        <div class="swiper-wrapper">
                            <?php foreach ($locations as $location) { ?>
                                <div class="swiper-slide swiper-office__slide">
                                    <h3 class="mb-3 card-office__title"><?= $location['location'] ?></h3>
                                    <p>
                                        <?= $location['address'] ?>
                                        <br>
                                        <?= $location['region'] ?>
                                    </p>
                                    <p class="card-office__contact">
                                        <?= $location['phone'] ?>
                                        <br>
                                        <?= $location['email'] ?>
                                    </p>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="swiper-office__navigation-container">
                            <div id="swiper-button-prev__office" class="swiper-button-prev swiper-button-custom--prev swiper-office__button"></div>
                            <div id="swiper-button-next__office" class="swiper-button-next swiper-button-custom--next swiper-office__button"></div>
                        </div>
                    </div>
                </div>
                <div id="officeDesktop" class="card card-office">
                    <h2 class="subtitle">Our Office</h2>
                    <div class="row">
                        <?php foreach ($locations as $location) { ?>
                            <div class="col-md-4">
                                <h3 class="mb-3 card-office__title"><?= $location['location'] ?></h3>
                                <p>
                                    <?= $location['address'] ?>
                                    <br>
                                    <?= $location['region'] ?>
                                </p>
                                <p class="card-office__contact">
                                    <?= $location['phone'] ?>
                                    <br>
                                    <?= $location['email'] ?>
                                </p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section-form">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 mb-5 mb-lg-0">
                        <h2 class="subtitle">Send Us A Message</h2>
                        <form action="#">
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <input class="form-control form-input" type="text" placeholder="Name" required>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <input class="form-control form-input" type="email" placeholder="Email" required>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <select class="form-control form-input" required>
                                        <option value="" disabled selected>What is this regarding?</option>
                                        <option value="1">Login Error</option>
                                        <option value="2">Payment Method</option>
                                        <option value="3">Location Not Found</option>
                                    </select>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <input class="form-control form-input" type="tel" placeholder="Phone (optional)">
                                </div>
                                <div class="col-12">
                                    <textarea class="form-control" rows="4" placeholder="Message" required></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mt-3">Send Message</button>
                        </form>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col">
                                <h2 class="subtitle">Our Location</h2>
                            </div>
                            <div class="col-auto">
                                <select class="form-control form-input">
                                    <?php foreach ($locations as $location) { ?>
                                        <option value="<?= $location['location'] ?>"><?= $location['location'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="map-container">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.104066007387!2d115.23826581460467!3d-8.681653193762244!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd240f88bb37a69%3A0xdf8443f1fabfe305!2sPT.Timedoor%20Indonesia!5e0!3m2!1sen!2sid!4v1621926072708!5m2!1sen!2sid" class="map" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php include '_footer.php' ?>
    <?php include '_global-script.php' ?>
    <script src="assets/js/contact.js"></script>
</body>

</html>