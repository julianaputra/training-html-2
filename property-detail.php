<?php
$images = [
    [
        "thumbnail" => "assets/img/property-detail/thumbnail/1.jpg",
        "master" => "assets/img/property-detail/1.jpg",
    ],
    [
        "thumbnail" => "assets/img/property-detail/thumbnail/2.jpg",
        "master" => "assets/img/property-detail/2.jpg",
    ],
    [
        "thumbnail" => "assets/img/property-detail/thumbnail/3.jpg",
        "master" => "assets/img/property-detail/3.jpg",
    ],
    [
        "thumbnail" => "assets/img/property-detail/thumbnail/4.jpg",
        "master" => "assets/img/property-detail/4.jpg",
    ],
    [
        "thumbnail" => "assets/img/property-detail/thumbnail/5.jpg",
        "master" => "assets/img/property-detail/5.jpg",
    ],
    [
        "thumbnail" => "assets/img/property-detail/thumbnail/6.jpg",
        "master" => "assets/img/property-detail/6.jpg",
    ],
    [
        "thumbnail" => "assets/img/property-detail/thumbnail/7.jpg",
        "master" => "assets/img/property-detail/7.jpg",
    ],
    [
        "thumbnail" => "assets/img/property-detail/thumbnail/8.jpg",
        "master" => "assets/img/property-detail/8.jpg",
    ]
];
$details = [
    [
        "key" => "Status",
        "value" => "Coming Soon"
    ],
    [
        "key" => "Property Type",
        "value" => "Apartment"
    ],
    [
        "key" => "Year Built",
        "value" => 1980
    ],
    [
        "key" => "Stories",
        "value" => 23
    ],
    [
        "key" => "Room Count",
        "value" => 6
    ],
    [
        "key" => "Parking Spaces",
        "value" => 2
    ],
];
$amenities = [
    [
        "icon" => "fas fa-wifi",
        "value" => "Internet",
    ],
    [
        "icon" => "fas fa-car",
        "value" => "Garage",
    ],
    [
        "icon" => "fas fa-wind",
        "value" => "Air Conditioning",
    ],
    [
        "icon" => "fas fa-soap",
        "value" => "Dishwasher",
    ],
    [
        "icon" => "fas fa-recycle",
        "value" => "Disposal",
    ],
    [
        "icon" => "fas fa-dungeon",
        "value" => "Balcony",
    ],
    [
        "icon" => "fas fa-dumbbell",
        "value" => "Gym",
    ],
    [
        "icon" => "far fa-smile",
        "value" => "Playroom",
    ],
    [
        "icon" => "fas fa-glass-martini-alt",
        "value" => "Bar",
    ],
];
$areas = [
    [
        "id" => 1,
        "title" => "transportation",
        "map" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.591250815591!2d115.20391151460444!3d-8.635180093794657!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd23f46af664051%3A0xf4022ebd24848749!2sTerminal%20Bus%20Ubung!5e0!3m2!1sen!2sid!4v1623003450088!5m2!1sen!2sid"
    ],
    [
        "id" => 2,
        "title" => "restaurants",
        "map" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.2710633218185!2d115.23146931460471!3d-8.665750993773372!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd2408a4df3884b%3A0x792d1d79cc11a3d2!2sMIE%20AYAM%20JAKARTA!5e0!3m2!1sen!2sid!4v1623003361317!5m2!1sen!2sid"
    ],
    [
        "id" => 3,
        "title" => "shopping",
        "map" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.3677136260794!2d115.21023061460464!3d-8.656534293779776!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd240a20b08cb11%3A0xc0600ec21e227e5e!2sBadung%20Market!5e0!3m2!1sen!2sid!4v1623003406613!5m2!1sen!2sid"
    ],
    [
        "id" => 4,
        "title" => "cafes & bars",
        "map" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3943.5433479929134!2d115.17754731460505!3d-8.734836993725237!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd246b27ec2dddd%3A0x8f59656238f75cfb!2sBoshe%20VVIP%20Club!5e0!3m2!1sen!2sid!4v1623003310618!5m2!1sen!2sid"
    ],
    [
        "id" => 5,
        "title" => "arts & entertainment",
        "map" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.3772800871225!2d115.2316827146045!3d-8.65562149378038!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd2408774ac0e19%3A0xee8eace99d65c91e!2sWerdhi%20Budaya%20Art%20Centre!5e0!3m2!1sen!2sid!4v1623003233805!5m2!1sen!2sid"
    ],
    [
        "id" => 6,
        "title" => "fitness",
        "map" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.5041651398196!2d115.18304571460452!3d-8.64350549378886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd238d238f980d1%3A0xbbb8ddcfeffd3af7!2sGYM%20GENeration!5e0!3m2!1sen!2sid!4v1623003273025!5m2!1sen!2sid"
    ]
];
$schools = [
    [
        "name" => "elementary",
        "schools" =>  [
            [
                "id" => 1,
                "school" => "Harvest Collegiate Elementary School",
                "type" => "Public",
                "grades" => "1-6",
                "rating" => 4
            ],
            [
                "id" => 2,
                "school" => "Xavier Elementary School",
                "type" => "Private",
                "grades" => "1-6",
                "rating" => 3
            ],
        ],
    ],
    [
        "name" => "middle",
        "schools" => [
            [
                "id" => 3,
                "school" => "Harvest Collegiate Middle School",
                "type" => "Public",
                "grades" => "7-9",
                "rating" => 5
            ],
            [
                "id" => 4,
                "school" => "Xavier Middle School",
                "type" => "Private",
                "grades" => "7-9",
                "rating" => 4
            ],
        ],
    ],
    [
        "name" => "high",
        "schools" => [
            [
                "id" => 5,
                "school" => "Harvest Collegiate High School",
                "type" => "Public",
                "grades" => "10-12",
                "rating" => 4
            ],
            [
                "id" => 6,
                "school" => "Xavier High School",
                "type" => "Private",
                "grades" => "10-12",
                "rating" => 5
            ],
        ]
    ]


]
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resideo | Property Listing</title>
    <?php include '_global-style.php' ?>
    <link rel="stylesheet" href="assets/css/pages/property-detail.css">
</head>

<body>
    <?php include '_header.php' ?>
    <main class="main bg-secondary">
        <section class="section-header pt-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <h1 class="section-header__title">Beautiful House in Marina</h1>
                        <p class="section-header__address">542 29th Avenue, Marina District, San Francisco, CA 94121</p>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="row">
                            <div class="col-auto">
                                <p class="section-header__price">$5,198,000</p>
                            </div>
                            <div class="col-auto">
                                <ul class="section-header__facility-container">
                                    <li class="section-header__facility-item">5 <span class="section-header__facility-unit">BD</span></li>
                                    <li class="section-header__facility-item">4 <span class="section-header__facility-unit">BA</span></li>
                                    <li class="section-header__facility-item">3,945 <span class="section-header__facility-unit">SF</span></li>
                                </ul>
                            </div>
                            <div class="col-auto">
                                <div class="section-header__buttons">
                                    <button class="btn button-form"><i class="fas fa-star"></i> Save</button>
                                    <button class="btn button-form"><i class="fas fa-share-alt"></i> Share</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fluid-container mt-4">
                <div class="row no-gutters">
                    <div class="col-12 col-lg-6">
                        <a href="<?= $images[0]['thumbnail'] ?>" data-rel="lightcase:myCollection">
                            <div class="section-header__image-container">
                                <img src="<?= $images[0]['thumbnail'] ?>" class="section-header__image" alt="">
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="row no-gutters">
                            <?php for ($i = 1; $i <= count($images) - 1; $i++) { ?>
                                <div class="col-6 section-header__small-side">
                                    <a href="<?= $images[$i]['thumbnail'] ?>" data-rel="lightcase:myCollection">
                                        <div class="section-header__image-container">
                                            <img src="<?= $images[$i]['thumbnail'] ?>" class="section-header__image" alt="">
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="pt-5 section-body">
            <div class="container">
                <div class="row">
                    <!-- Left Section -->
                    <div class="col-lg-8">
                        <!-- Key Details -->
                        <section class="section-detail">
                            <h2 class="subtitle">Key Details</h2>
                            <div class="row">
                                <?php for ($i = 0; $i < count($details); $i++) { ?>
                                    <div class="col-sm-6 section-detail__detail-item">
                                        <hr class="separatorDetails">
                                        <p class="section-detail__detail-key"><?= $details[$i]["key"] ?></p>
                                        <p class="section-detail__detail-value"><?= $details[$i]["value"] ?></p>
                                    </div>
                                <?php } ?>
                            </div>
                        </section>
                        <!-- Overview -->
                        <section class="section-overview my-5">
                            <h2 class="subtitle">Overview</h2>
                            <div id="overviewContainer" class="section-overview__desc-container">
                                <p id="overviewContent" class="section-overview__desc">
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Natus earum reprehenderit rem, veniam commodi molestiae id aperiam suscipit ab quidem ex tempore adipisci fugiat, cumque eius doloribus vero accusantium. Labore.
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Natus earum reprehenderit rem, veniam commodi molestiae id aperiam suscipit ab quidem ex tempore adipisci fugiat, cumque eius doloribus vero accusantium. Labore.
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vitae natus blanditiis accusamus inventore earum quae dolor voluptate sequi et molestias modi sint, maiores corporis non dolore, sapiente illum ut rem?
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vitae natus blanditiis accusamus inventore earum quae dolor voluptate sequi et molestias modi sint, maiores corporis non dolore, sapiente illum ut rem?
                                </p>
                            </div>
                            <button id="overviewButton" class="btn section-overview__button mt-3">
                                Continue Reading <i class="fas fa-angle-down"></i>
                            </button>
                        </section>
                        <!-- Amenities -->
                        <section class="section-amenities my-5">
                            <h2 class="subtitle">Amenities</h2>
                            <div class="row">
                                <?php for ($i = 0; $i < count($amenities); $i++) { ?>
                                    <div class="col-sm-4 section-detail__detail-item">
                                        <hr class="separatorAmenities">
                                        <p class="m-0"><i class="<?= $amenities[$i]["icon"] ?>"></i> <?= $amenities[$i]["value"] ?></p>
                                    </div>
                                <?php } ?>
                            </div>
                        </section>
                        <!-- Explore Area -->
                        <section class="section-area my-5">
                            <h2 class="subtitle">Explore the Area</h2>
                            <div class="nav-pills-custom">
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <?php for ($i = 0; $i < count($areas); $i++) { ?>
                                        <li class="nav-item section-area__nav-item" role="presentation">
                                            <a class="nav-link <?php if ($i == 0) echo 'active' ?>" id="<?= $areas[$i]["id"] ?>" data-toggle="pill" href="#pills-<?= $areas[$i]["id"] ?>" role="tab"><?= $areas[$i]["title"] ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content accordion" id="pills-tabContent">
                                    <?php for ($i = 0; $i < count($areas); $i++) { ?>
                                        <div class="card tab-pane fade <?php if ($i == 0) echo 'active show' ?>" id="pills-<?= $areas[$i]["id"] ?>" role="tabpanel">
                                            <div class="card-header" role="tab" id="heading-<?= $areas[$i]["id"] ?>">
                                                <!-- Note: `data-parent` removed from here -->
                                                <a class="card-title <?php if ($areas[$i] != $areas[0]) echo 'collapsed' ?>" data-toggle="collapse" href="#collapse-<?= $areas[$i]["id"] ?>" aria-expanded="<?php if ($areas[$i] == $areas[0]) echo 'true' ?>">
                                                    <?= $areas[$i]["title"] ?>
                                                </a>
                                            </div>
                                            <!-- Note: New place of `data-parent` -->
                                            <div id="collapse-<?= $areas[$i]["id"] ?>" class="collapse <?php if ($areas[$i] == $areas[0]) echo 'show' ?>" data-parent="#pills-tabContent" role="tabpanel">
                                                <iframe src="<?= $areas[$i]["map"] ?>" class="section-area__map" allowfullscreen="" loading="lazy"></iframe>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </section>
                        <!-- Payment -->
                        <section class="section-payment my-5">
                            <h2 class="subtitle">Payment Calculator</h2>
                            <div class="row align-items-center justify-content-center">
                                <div class="col-12 col-sm-4">
                                    <div class="payment-chart">
                                        <div class="payment-chart__text-container">
                                            <p class="payment-chart__total">$26,669</p>
                                            <p class="payment-chart__unit">per month</p>
                                        </div>
                                        <canvas id="paymentChart"></canvas>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="table-responsive my-4">
                                        <table class="table table-borderless text-nowrap">
                                            <tbody>
                                                <tr class="border-bottom">
                                                    <td><span class="payment-chart__indicator"><i class="fas fa-minus"></i></span> Principal and Interest</td>
                                                    <td class="text-right"><b>$23,565</b></td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td><span class="payment-chart__indicator"><i class="fas fa-minus"></i></span> Property Taxes</td>
                                                    <td class="text-right"><b>$1,068</b></td>
                                                </tr>
                                                <tr>
                                                    <td><span class="payment-chart__indicator"><i class="fas fa-minus"></i></span> HOA Dues</td>
                                                    <td class="text-right"><b>$2,036</b></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <form class="row mt-4">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="term" class="form__label">Term</label>
                                        <select class="form-control" id="term">
                                            <option>10 Years Fixed</option>
                                            <option>20 Years Fixed</option>
                                            <option>30 Years Fixed</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="interest" class="form__label">Interest</label>
                                        <input type="text" value="4.45%" class="form-control bg-white" id="interest" disabled>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="home-price" class="form__label">Home Price</label>
                                        <input type="text" value="$5,198,000" class="form-control bg-white" id="home-price" disabled>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="down-payment" class="form__label">Down Payment</label>
                                        <div class="row">
                                            <div class="col-8">
                                                <input type="text" class="form-control bg-white" id="down-payment" value="$519,800" disabled>
                                            </div>
                                            <div class="col-4">
                                                <input type="text" class="form-control bg-white" id="down-payment-percent" value="10%" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </section>
                        <!-- School -->
                        <section class="section-area my-5">
                            <h2 class="subtitle">Schools in Marina District</h2>
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <?php for ($i = 0; $i < count($schools); $i++) { ?>
                                    <li class="nav-item section-area__nav-item" role="presentation">
                                        <a class="nav-link <?php if ($i == 0) echo 'active' ?>" id="<?= $schools[$i]["name"] ?>" data-toggle="pill" href="#pills-<?= $schools[$i]["name"] ?>" role="tab"><?= $schools[$i]["name"] ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <?php for ($i = 0; $i < count($schools); $i++) { ?>
                                    <div class="tab-pane fade <?php if ($i == 0) echo 'active show' ?> " id="pills-<?= $schools[$i]["name"] ?>" role="tabpanel">
                                        <div class="table-responsive">
                                            <table class="table table-borderless m-0">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">School</th>
                                                        <th scope="col">Type</th>
                                                        <th scope="col">Grades</th>
                                                        <th scope="col">Rating</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($schools[$i]["schools"] as $school) { ?>
                                                        <tr class="border-top">
                                                            <td><?= $school["school"] ?></td>
                                                            <td><?= $school["type"] ?></td>
                                                            <td><?= $school["grades"] ?></td>
                                                            <td>
                                                                <?= $school["rating"] ?>/5
                                                                <span class="table-school__star-container">
                                                                    <?php for ($j = 0; $j < $school["rating"]; $j++) { ?>
                                                                        <i class="fas fa-star"></i>
                                                                    <?php } ?>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </section>
                    </div>
                    <!-- Right Section -->
                    <div class="col-lg-4">
                        <div class="section-detail__right-side">
                            <h2 class="subtitle">Listed By</h2>
                            <div class="media-user row">
                                <div class="col-sm-5">
                                    <div class="media-user__image-container mb-4">
                                        <img class="media-user__image" src="assets/img/agent-4.jpg" alt="Generic placeholder image">
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="media-user__body">
                                        <h3 class="media-user__title">Erika Tillman</h3>
                                        <div class="media-user__star-container">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <p class="media-user__email">erika.tillman@resideo.com</p>
                                        <p class="media-user__phone"><i class="fas fa-phone-alt"></i> (123) 456-7890</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row normal-buttons no-gutters mt-3">
                                <div class="col-sm-6 pr-sm-1"><a href="javascript:void(0)" class="btn btn-sm btn-primary font-weight-bold text-uppercase w-100"><i class="far fa-envelope"></i> Contact Agent</a></div>
                                <div class="col-sm-6 pl-sm-1"><a href="javascript:void(0)" class="btn btn-sm button-form border w-100"><i class="far fa-calendar-check"></i> Request Tour</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="floating-buttons border-top">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-6 pr-1"><a href="javascript:void(0)" class="btn btn-primary font-weight-bold text-uppercase w-100"><i class="far fa-envelope"></i> Contact Agent</a></div>
                    <div class="col-6 pl-1"><a href="javascript:void(0)" class="btn button-form border w-100"><i class="far fa-calendar-check"></i> Request Tour</a></div>
                </div>
            </div>
        </div>
    </main>
    <?php include '_footer.php' ?>
    <?php include '_global-script.php' ?>
    <script src="assets/js/property-detail.js"></script>
</body>

</html>