<?php
$properties = [
    [
        "title" => "Chich Apartment in Downtown",
        "price" => "$890,000",
        "facility" => "2 BD | 2 BA | 920 SF",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "image" => "assets/img/prop-4-1-gallery.jpg"
    ],
    [
        "title" => "Colorful Little Apartment",
        "price" => "$2,674",
        "facility" => "1 BD | 1 BA | 500 SF",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "image" => "assets/img/prop-5-1-gallery.jpg"
    ],
    [
        "title" => "Cozy Two Bedroom Apartment",
        "price" => "$960,000",
        "facility" => "2 BD | 2 BA | 870 SF",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "image" => "assets/img/prop-6-1-gallery.jpg"
    ],
    [
        "title" => "Beautiful House in Marina",
        "price" => "$5,198,000",
        "facility" => "5 BD | 4.5 BA | 3,945 SF",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "image" => "assets/img/prop-10-1-gallery.jpg"
    ],
    [
        "title" => "Modern Residence",
        "price" => "$7,995",
        "facility" => "4 BD | 1.5 BA | 2,240 SF",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "image" => "assets/img/prop-11-1-gallery.jpg"
    ],
    [
        "title" => "Luxury Manison",
        "price" => "$5,430,000",
        "facility" => "4 BD | 5 BA | 5,200 SF",
        "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam ipsam voluptatibus exercitationem error, beatae non nostrum sit minima, voluptate porro quod iste expedita explicabo quidem iure dolores enim, fugiat cupiditate!",
        "image" => "assets/img/prop-12-1-gallery.jpg"
    ],
];

$agents = [
    [
        "name" => "Scott Goodwin",
        "phone" => "(123) 456-7890",
        "image" => "assets/img/agent-1.jpg"
    ],
    [
        "name" => "Alayna Becker",
        "phone" => "(456) 123-7890",
        "image" => "assets/img/agent-2.jpg"
    ],
    [
        "name" => "Melvin Blackwell",
        "phone" => "(789) 123-4560",
        "image" => "assets/img/agent-3.jpg"
    ],
    [
        "name" => "Erika Tillman",
        "phone" => "(890) 456-1237",
        "image" => "assets/img/agent-4.jpg"
    ],
];

$benefits = [
    [
        "title" => "Find your future home",
        "desc" => "We help you find a new home by offering a smart real estate experience",
        "image" => "assets/img/service-icon-1-white.svg"
    ],
    [
        "title" => "Experienced agents",
        "desc" => "Find an agent who knows your market best",
        "image" => "assets/img/service-icon-2-white.svg"
    ],
    [
        "title" => "Buy or rent homes",
        "desc" => "Millions of houses and apartments in your favourite cities",
        "image" => "assets/img/service-icon-3-white.svg"
    ],
    [
        "title" => "Find your future home",
        "desc" => "We help you find a new home by offering a smart real estate experience",
        "image" => "assets/img/service-icon-4-white.svg"
    ],
]
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resideo | Training</title>
    <?php include '_global-style.php' ?>
    <link rel="stylesheet" href="assets/css/pages/home.css">
    <link rel="stylesheet" href="assets/css/components/card-property.css">
</head>

<body>
    <?php include '_header.php' ?>
    <main>
        <!-- Hero Section -->
        <section class="section-hero">
            <div class="swiper-container swiper-hero">
                <div class="swiper-wrapper">
                    <?php foreach ($properties as $property) { ?>
                        <div class="swiper-slide">
                            <div class="container h-100 d-flex">
                                <div class="swiper-hero__text-container">
                                    <h1 class="swiper-hero__title"><?= $property['title'] ?></h1>
                                    <p class="swiper-hero__facility"><?= $property['facility'] ?></p>
                                    <p class="swiper-hero__price"><?= $property['price'] ?></p>
                                    <a href="property-detail.php" class="button">View Details</a>
                                </div>
                                <img class="swiper-hero__image" src="<?= $property['image'] ?>" alt="<?= $property['title'] . ' Banner Image' ?>">
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="swiper-pagination swiper-pagination-custom swiper-hero__pagination"></div>
            <div class="container swiper-hero__navigation-container">
                <div id="swiper-button-prev__hero" class="swiper-button-prev swiper-button-custom--prev"></div>
                <div id="swiper-button-next__hero" class="swiper-button-next swiper-button-custom--next"></div>
            </div>
        </section>
        <!-- Property Section -->
        <section class="section section-property">
            <div class="container">
                <h2 class="section__title">Featured Properties</h2>
                <p class="section__desc">Browse our latest hot offers</p>
                <div class="row">
                    <?php foreach ($properties as $property) { ?>
                        <div class="col-12 col-md-6 col-lg-4">
                            <a href="property-detail.php" class="card card-property">
                                <div class="card-property__image-container">
                                    <img class="card-property__image" src="<?= $property['image'] ?>" alt="<?= $property['title'] . ' Image' ?>">
                                </div>
                                <div class="card-body card-property__body">
                                    <h3 class="card-property__title"><?= $property['title'] ?></h3>
                                    <p class="card-property__price"><?= $property['price'] ?></p>
                                    <p class="card-property__facility"><?= $property['facility'] ?></p>
                                    <p class="card-property__desc"><?= $property['desc'] ?></p>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <a href="property-listing.php" class="button button--primary mt-3">Browse All</a>
            </div>
        </section>
        <!-- Benefit Section -->
        <section class="section-benefit">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <h2 class="section__title section-benefit__title">Why Choose Us</h2>
                        <p class="section__desc m-0">We offer perfect real estate services</p>
                        <a href="#" class="button section-benefit__button">Learn More</a>
                    </div>
                </div>
                <div class="swiper-container swiper-benefit">
                    <div class="swiper-wrapper">
                        <?php foreach ($benefits as $benefit) { ?>
                            <div class="swiper-slide">
                                <div class="card card-benefit">
                                    <div class="card-body">
                                        <img class="card-benefit__image" src="<?= $benefit["image"] ?>" alt="<?= $benefit["title"] ?>">
                                        <h3 class="card-benefit__title"><?= $benefit["title"] ?></h3>
                                        <p class="card-benefit__desc"><?= $benefit["desc"] ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="container swiper-benefit__navigation-container">
                <div id="swiper-button-prev__benefit" class="swiper-button-prev swiper-button-custom--prev swiper-benefit__button"></div>
                <div id="swiper-button-next__benefit" class="swiper-button-next swiper-button-custom--next swiper-benefit__button"></div>
            </div>
        </section>
        <!-- Agent Section -->
        <section class="section section-agent">
            <div class="container">
                <h2 class="section__title">Our Featured Agents</h2>
                <p class="section__desc">Meet the best real estate agents</p>
                <div class="row justify-content-center">
                    <?php foreach ($agents as $agent) { ?>
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="card card-agent">
                                <div class="card-agent__image-container">
                                    <img class="card-agent__image" src="<?= $agent['image'] ?>" alt="<?= $agent['name'] . ' Image' ?>">
                                </div>
                                <div class="card-agent__body">
                                    <h3 class="card-agent__name"><?= $agent['name'] ?></h3>
                                    <p class="card-agent__phone"><?= $agent['phone'] ?></p>
                                    <div class="card-agent__social">
                                        <a href="#" class="card-agent__social-link"><i class="fab fa-instagram"></i></a>
                                        <a href="#" class="card-agent__social-link"><i class="fab fa-facebook-square"></i></a>
                                        <a href="#" class="card-agent__social-link"><i class="fab fa-twitter"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <a href="#" class="button button--primary mt-3">See All Agents</a>
            </div>
        </section>
        <!-- Search Section -->
        <section class="section-search">
            <div class="card-search">
                <div class="card-body">
                    <h2 class="card-search__title">Search Smarter, From Anywhere</h2>
                    <p class="card-search__desc">Power your search with our Resideo real estate platform, for timely listings and a seamless experience.</p>
                    <a href="#" class="button mt-3">Search Now</a>
                </div>
            </div>
        </section>
    </main>
    <?php include '_footer.php' ?>
    <?php include '_global-script.php' ?>
    <script src="assets/js/home.js"></script>
</body>

</html>